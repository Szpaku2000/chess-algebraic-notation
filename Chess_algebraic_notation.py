columns_dictionary = {
    "a": 0,
    "b": 1,
    "c": 2,
    "d": 3,
    "e": 4,
    "f": 5,
    "g": 6,
    "h": 7
}

rows_dictionary = {
    "8": 0,
    "7": 1,
    "6": 2,
    "5": 3,
    "4": 4,
    "3": 5,
    "2": 6,
    "1": 7
}


class LongAlgebraic:

    def __init__(self, input_string):
        output = input_string.split("\n")
        for i in range(len(output)):
            output[i] = output[i].split(" ")
            if "" in output[i]:
                output[i].remove("")

        count = 1
        for move in output:
            move[0] = count
            count += 1

        self.moves = output


class FEN:

    def __init__(self):
        fen_initial_state = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"  # initial board state
        fen_state = fen_initial_state.split(" ")
        fen_state[0] = fen_state[0].split("/")
        fen_state[0][2] = "        "
        fen_state[0][3] = "        "
        fen_state[0][4] = "        "
        fen_state[0][5] = "        "
        new_rows = list()
        for row in fen_state[0]:
            new_rows.append(list(row))
        fen_state[0] = new_rows

        self.board_state = fen_state[0]
        self.active_colour = fen_state[1]
        self.castling_rights = fen_state[2]
        self.en_passant = fen_state[3]
        self.halfmoves = int(fen_state[4])
        self.fullmoves = fen_state[5]

    def __str__(self):
        output = self.board_state_str() + " "
        output += self.active_colour + " "
        output += self.castling_rights + " "
        output += self.en_passant + " "
        output += str(self.halfmoves) + " "
        output += str(self.fullmoves)
        return output

    def update_piece(self, move):
        if self.castling_rights == "":
            self.castling_rights = "-"
        if "-" in move and move.count("O") == 2:
            self.castling_short()
        elif "-" in move and move.count("O") == 3:
            self.castling_long()
        elif move.islower():
            self.pawn_move(move)
        elif "=" in move:
            self.pawn_promotion(move)
        elif move[0] == "R":
            self.rook_move(move)
        elif move[0] == "N":
            self.knight_move(move)
        elif move[0] == "B":
            self.bishop_move(move)
        elif move[0] == "Q":
            self.queen_move(move)
        elif move[0] == "K":
            self.king_move(move)
        else:
            print("Error: Incorrect move")

    def king_move(self, move):
        # taking under consideration active colour
        # taking under consideration if the move is capture
        # delete piece from the board
        # move it to a new place

        if self.active_colour == "w":
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "K"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "K"
                self.update_halfmove("increment")
            self.castling_rights = self.castling_rights.replace("K", "")
            self.castling_rights = self.castling_rights.replace("Q", "")
        else:
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "k"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "k"
                self.update_halfmove("increment")
            self.castling_rights = self.castling_rights.replace("k", "")
            self.castling_rights = self.castling_rights.replace("q", "")

        self.reset_en_passant()
        self.update_active_color()

    def queen_move(self, move):
        if self.active_colour == "w":
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "Q"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "Q"
                self.update_halfmove("increment")
        else:
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "q"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "q"
                self.update_halfmove("increment")

        self.update_active_color()
        self.reset_en_passant()

    def bishop_move(self, move):
        if self.active_colour == "w":
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "B"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "B"
                self.update_halfmove("increment")
        else:
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "b"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "b"
                self.update_halfmove("increment")

        self.reset_en_passant()
        self.update_active_color()

    def knight_move(self, move):
        if self.active_colour == "w":
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "N"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "N"
                self.update_halfmove("increment")
        else:
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "n"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "n"
                self.update_halfmove("increment")

        self.reset_en_passant()
        self.update_active_color()

    def rook_move(self, move):
        if self.active_colour == "w":
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "R"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "R"
                self.update_halfmove("increment")
            if move[1] == "a":
                self.castling_rights = self.castling_rights.replace("Q", "")
            elif move[1] == "h":
                self.castling_rights = self.castling_rights.replace("K", "")
        else:
            if "x" in move:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[5]]][columns_dictionary[move[4]]] = "r"
                self.update_halfmove("zero")
            else:
                self.board_state[rows_dictionary[move[2]]][columns_dictionary[move[1]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "r"
                self.update_halfmove("increment")
            if move[1] == "a":
                self.castling_rights = self.castling_rights.replace("q", "")
            elif move[1] == "h":
                self.castling_rights = self.castling_rights.replace("k", "")

        self.reset_en_passant()
        self.update_active_color()

    def pawn_move(self, move):
        if self.active_colour == "w":
            if "x" in move:
                if self.en_passant == "-":
                    pawn_taken_in_en_passant = self.en_passant
                else:
                    pawn_taken_in_en_passant = self.en_passant[0] + str(int(self.en_passant[1]))
                if pawn_taken_in_en_passant in move:
                    self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                    self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "P"
                    self.board_state[rows_dictionary["5"]][columns_dictionary[self.en_passant[0]]] = " "
                    self.reset_en_passant()
                else:
                    self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                    self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "P"
            else:
                self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                self.board_state[rows_dictionary[move[3]]][columns_dictionary[move[2]]] = "P"
        else:
            if "x" in move:
                if self.en_passant == "-":
                    pawn_taken_in_en_passant = self.en_passant
                else:
                    pawn_taken_in_en_passant = self.en_passant[0] + str(int(self.en_passant[1]))
                if pawn_taken_in_en_passant in move:
                    self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                    self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "p"
                    self.board_state[rows_dictionary["4"]][columns_dictionary[self.en_passant[0]]] = " "
                    self.reset_en_passant()
                else:
                    self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                    self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = "p"
            else:
                self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                self.board_state[rows_dictionary[move[3]]][columns_dictionary[move[2]]] = "p"

        self.check_en_passant(move)
        self.update_active_color()
        self.update_halfmove("zero")

    def pawn_promotion(self, move):
        promotion_piece = " "
        for letter in move:
            if letter.isupper():
                promotion_piece = letter

        if self.active_colour == "w":
            if "x" in move:
                self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = promotion_piece.upper()
            else:
                self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                self.board_state[rows_dictionary[move[3]]][columns_dictionary[move[2]]] = promotion_piece.upper()
        else:
            if "x" in move:
                self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                self.board_state[rows_dictionary[move[4]]][columns_dictionary[move[3]]] = promotion_piece.lower()
            else:
                self.board_state[rows_dictionary[move[1]]][columns_dictionary[move[0]]] = " "
                self.board_state[rows_dictionary[move[3]]][columns_dictionary[move[2]]] = promotion_piece.lower()

        self.update_active_color()
        self.update_halfmove("zero")

    def check_en_passant(self, move):
        if "x" in move:
            self.reset_en_passant()
            return

        if self.active_colour == "w":
            if abs(int(move[1]) - int(move[3])) == 2:
                if columns_dictionary[move[0]] == 0:  # edge of the board cases - checking if is not outside an array
                    if self.board_state[rows_dictionary["4"]][columns_dictionary[move[0]] + 1] == "p":
                        self.en_passant = move[0] + "3"
                elif columns_dictionary[move[0]] == 7:
                    if self.board_state[rows_dictionary["4"]][columns_dictionary[move[0]] - 1] == "p":
                        self.en_passant = move[0] + "3"
                else:
                    if self.board_state[rows_dictionary["4"]][columns_dictionary[move[0]] - 1] == "p":
                        self.en_passant = move[0] + "3"
                    elif self.board_state[rows_dictionary["4"]][columns_dictionary[move[0]] + 1] == "p":
                        self.en_passant = move[0] + "3"
            else:
                self.reset_en_passant()
        else:
            if abs(int(move[1]) - int(move[3])) == 2:
                if columns_dictionary[move[0]] == 0:  # edge of the board cases - checking if is not outside an array
                    if self.board_state[rows_dictionary["5"]][columns_dictionary[move[0]] + 1] == "P":
                        self.en_passant = move[0] + "6"
                elif columns_dictionary[move[0]] == 7:
                    if self.board_state[rows_dictionary["5"]][columns_dictionary[move[0]] - 1] == "P":
                        self.en_passant = move[0] + "6"
                else:
                    if self.board_state[rows_dictionary["5"]][columns_dictionary[move[0]] - 1] == "P":
                        self.en_passant = move[0] + "6"
                    elif self.board_state[rows_dictionary["5"]][columns_dictionary[move[0]] + 1] == "P":
                        self.en_passant = move[0] + "6"
            else:
                self.reset_en_passant()

    def reset_en_passant(self):
        self.en_passant = "-"

    def castling_short(self):
        if self.active_colour == "w":
            self.board_state[7][4] = " "
            self.board_state[7][5] = "R"
            self.board_state[7][6] = "K"
            self.board_state[7][7] = " "
            self.castling_rights = self.castling_rights.replace("K", "")
            self.castling_rights = self.castling_rights.replace("Q", "")
        else:
            self.board_state[0][4] = " "
            self.board_state[0][5] = "r"
            self.board_state[0][6] = "k"
            self.board_state[0][7] = " "
            self.castling_rights = self.castling_rights.replace("k", "")
            self.castling_rights = self.castling_rights.replace("q", "")

        self.reset_en_passant()
        self.update_halfmove("increment")
        self.update_active_color()

    def castling_long(self):
        if self.active_colour == "w":
            self.board_state[7][0] = " "
            self.board_state[7][1] = " "
            self.board_state[7][2] = "K"
            self.board_state[7][3] = "R"
            self.board_state[7][4] = " "
            self.castling_rights = self.castling_rights.replace("K", "")
            self.castling_rights = self.castling_rights.replace("Q", "")
        else:
            self.board_state[0][0] = " "
            self.board_state[0][1] = " "
            self.board_state[0][2] = "k"
            self.board_state[0][3] = "r"
            self.board_state[0][4] = " "
            self.castling_rights = self.castling_rights.replace("k", "")
            self.castling_rights = self.castling_rights.replace("q", "")

        self.reset_en_passant()
        self.update_halfmove("increment")
        self.update_active_color()

    def update_active_color(self):
        if self.active_colour == "w":
            self.active_colour = "b"
        elif self.active_colour == "b":
            self.active_colour = "w"
        else:
            print("Error: Wrong active colour")
            exit(-1)

    def update_halfmove(self, option):
        if option == "increment":
            self.halfmoves += 1
        elif option == "zero":
            self.halfmoves = 0

    def update_fullmove(self, move_number):
        self.fullmoves = move_number

    def board_state_str(self):
        output = ""
        for row in self.board_state:
            for column in row:
                output = output + column
            output = output + "/"
        output = output.replace("        ", "8")
        output = output.replace("       ", "7")
        output = output.replace("      ", "6")
        output = output.replace("     ", "5")
        output = output.replace("    ", "4")
        output = output.replace("   ", "3")
        output = output.replace("  ", "2")
        output = output.replace(" ", "1")
        return output


def algebraic_to_fen(algebraic, fen):
    for element in algebraic.moves:
        fen.update_fullmove(element[0])
        if len(element) == 2:
            fen.update_piece(element[1])
        elif len(element) == 3:
            fen.update_piece(element[1])
            fen.update_piece(element[2])
        else:
            print("Wrong element in Algebraic notation!")
            exit(-1)

    return fen


# FEN initial state: rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1

# this is correctly noted game of chess so it should result in a proper state in FEN
# 1. e4 e6 2. Nf3 Bb4 3. Qe2 Nf6 4. c3 O-O 5. cxb4 Nxe4 6. d4 Re8 7. b5 c5 8. bxc6 dxc6 9. Bh6 gxh6 10. Nc3 Nxc3 11. bxc3 Qd5 12. O-O-O Qg5+ 13. Nxg5 e5 14. d5 e4 15. d6 Bg4 16. d7 f5 17. dxe8=Q+ Kg7 18. Qf7+ Kh8 19. Qxh7#
# the same game but in long algebraic notation
# 1. e2e4 e7e6 2. Ng1f3 Bf8b4 3. Qd1e2 Ng8f6 4. c2c3 O-O 5. c3xb4 Nf6xe4 6. d2d4 Rf8e8 7. b4b5 c2c5 8. b5xc6 d7xc6 9. Bc1h6 g7xh6 10. Nb1c3 Ne4xc3 11. b2xc3 Qd8d5 12. O-O-O Qd5g5+ 13. Nf3xg5 e6e5 14. d4d5 e5e4 15. d5d6 Bc8g4 16. d6d7 f7f5 17. d7xe8=Q+ Kg8g7 18. Qe8f7+ Kg7h8 19. Qf7xh7#
# example containing most possible moves

# en passant pawn check
# 1. h4 a5 2. h5 a4 3. b4 axb3 4. e4 g5 5. hxg6 b2 6. e5 d5 7. exd6
# example_algebraic = "1. h2h4 a7a5 \n2. h4h5 a5a4 \n3. b2b4 a4xb3 \n4. e2e4 g7g5 \n5. h5xg6 b3b2 \n6. e4e5 d7d5 \n7. e5xd6 "

example_algebraic = "1. e2e4 e7e6 \n2. Ng1f3 Bf8b4 \n3. Qd1e2 Ng8f6 \n4. c2c3 O-O \n5. c3xb4 Nf6xe4 \n6. d2d4 Rf8e8 " \
                    "\n7. b4b5 c7c5 \n8. b5xc6 d7xc6 \n9. Bc1h6 g7xh6 \n10. Nb1c3 Ne4xc3 \n11. b2xc3 Qd8d5 \n12. " \
                    "O-O-O Qd5g5+ \n13. Nf3xg5 e6e5 \n14. d4d5 e5e4 \n15. d5d6 Bc8g4 \n16. d6d7 f7f5 \n17. d7xe8=Q+ " \
                    "Kg8g7 \n18. Qe8f7+ Kg7h8 \n19. Qf7xh7# "

my_fen = FEN()
my_algebraic = LongAlgebraic(example_algebraic)

for row in my_fen.board_state:
    print(row)

algebraic_to_fen(my_algebraic, my_fen)

for move in my_algebraic.moves:
    print(f"{move}")
print(my_fen)
